from golang:1.15

WORKDIR /go/src/maratona-fullcycle-4

ADD . .

RUN go build main.go

EXPOSE 8080

ENTRYPOINT ["./main"]