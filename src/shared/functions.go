package shared

import "log"

// HandleErrors is a function to show errors log
func HandleErrors(errorVar error) {
	if errorVar != nil {
		log.Println(errorVar.Error())
	}
}
