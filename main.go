package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/baugoncalves/maratona-fullcycle-4/src/middlewares"
	"gitlab.com/baugoncalves/maratona-fullcycle-4/src/shared"
)

func majorRoute(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	encoder.Encode("Eu sou Full Cycle")
}

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", majorRoute)
}

func main() {
	var errorServer error
	var router *mux.Router

	log.Println("Server started on: http://localhost:8080")

	router = mux.NewRouter()
	router.Use(middlewares.CORS)

	setRoutes(router)

	errorServer = http.ListenAndServe(":8080", router)
	shared.HandleErrors(errorServer)
}
